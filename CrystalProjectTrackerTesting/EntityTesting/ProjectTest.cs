﻿using CrystalProjectTracker.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CrystalProjectTrackerTesting.EntityTesting
{
    [TestClass]
    public class ProjectTest
    {
        Project project = new Project();
        [TestInitialize]
        public void Setup()
        {
            project.project_id = 1;
            project.project_name = "Test Project";
            project.project_status = 1;
            project.project_description = "Short description of Project";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectNameCannotBeEmpty()
        {
            project.project_name = "";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectNameShouldNotBeNull()
        {
            project.project_name = null;
        }
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectNameShouldBeValid()
        {
            project.project_name = "  ";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectDescriptionCannotBeEmpty()
        {
            project.project_description = "";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectDescriptionShouldNotBeNull()
        {
            project.project_description = null;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectDescriptionShouldBeValid()
        {
            project.project_description = "   ";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectStatusShouldNotBeLessThanZero()
        {
            project.project_status = -1;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ProjectStatusShouldNotBeGreaterThanOne()
        {
            project.project_status = 5;
        }
    }
}
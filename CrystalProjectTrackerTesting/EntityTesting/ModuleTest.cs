﻿using CrystalProjectTracker.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrystalProjectTrackerTesting.EntityTesting
{
    [TestClass]
    public class ModuleTest
    {
        Module module = new Module();
        [TestInitialize]
        public void setup()
        {
            module.module_id = 1;
            module.module_name = "Test Module";
            module.module_status = 1;
            module.module_description = "This is a tets Module.";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ModuleNameShouldNotBeEmpty()
        {
            module.module_name = "";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ModuleNameShouldNotBeNull()
        {
            module.module_name = null;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_Module_nameShouldBeValid()
        {
            module.module_name = "  ";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Tets_ModuleDescriptionShouldNotEmpty()
        {
            module.module_description = "";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ModuleDescriptionShouldNotBeNull()
        {
            module.module_description = null;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ModuleDescriptionShouldBeValid()
        {
            module.module_description = "  ";
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ModuleStatusShouldNotBeLessThanZero()
        {
            module.module_status = -1;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Test_ModuleStatusShouldNotBeGreaterThanOne()
        {
            module.module_status = 5;
        }
    }
}

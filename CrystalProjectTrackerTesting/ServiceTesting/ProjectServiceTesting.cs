﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Services.Assembler;
using CrystalProjectTracker.Services.DTO;
using CrystalProjectTracker.Services.ProjectService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;



namespace CrystalProjectTrackerTesting.ServiceTesting
{
    [TestClass]
    public class ProjectServiceTesting
    {
        Mock<IProjectRepository> IProjectRepository = new Mock<IProjectRepository>();
        private Mock<TransactionScopeHelperInterface> transactionScopeHelper = new Mock<TransactionScopeHelperInterface>();
        ProjectAssembler projectAssembler = new ProjectAssembler();


        ProjectDTO project_dto;
        Project project;
        ProjectService projectService;

        [TestInitialize]
        public void Setup()
        {
            projectService = new ProjectService(IProjectRepository.Object, transactionScopeHelper.Object,projectAssembler);
            project_dto = new ProjectDTO()
            {
                project_id = 1,
                project_name = "Ticket Management System",
                project_status = 1,
                project_description = "Blakvdvdvdvhd duhviduv ov edov ueovi v voi uvc",
            };

            project = new Project()
            {
                project_id = 1,
                project_name = "Ticket Management System",
                project_status = 1,
                project_description = "Blakvdvdvdvhd duhviduv ov edov ueovi v voi uvc",
            };
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Create_WhenCalled_ThrowProjectNameAlreadyExistsExceptionWhenProjectWithSameNameAlreadyExists()
        {
            IProjectRepository.Setup(a => a.GetProjectByName(It.IsAny<string>())).Returns(new Project());
            projectService.Create(project_dto);
        }

        [TestMethod]
        public void Create_WhenCalled_TestThatDataWillInsertWhenProjectNameIsNull()
        {
            IProjectRepository.Setup(a => a.GetProjectByName(null));
            projectService.Create(project_dto);
        }

        [TestMethod]
        public void Create_WhenCalled_TestThatDataWillSavedIntoDatabaseOrNot()
        {
            IProjectRepository.Setup(a => a.insert(It.IsAny<Project>())).Verifiable();
            projectService.Create(project_dto);
            IProjectRepository.Verify(a => a.insert(It.IsAny<Project>()), Times.Exactly(1));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Upadte_WhenCalled_ThrowProjectNameAlreadyExistsExceptionWhenProjectWithThatNameAlreadyExists()
        {
            IProjectRepository.Setup(a => a.GetProjectByName(It.IsAny<string>())).Returns(new Project());
            projectService.Update(project_dto);
        }

        [TestMethod]
        public void Update_WhenCalled_TestThatDataWillUpdateIntoDatabaseOrNot()
        {
            IProjectRepository.Setup(a => a.getById(It.IsAny<int>())).Returns(project);
            projectService.Update(project_dto);
            IProjectRepository.Verify(a => a.update(It.IsAny<Project>()),Times.Exactly(1));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Delete_WhenCalled_ThrowAnInvalidProjectIdExceptionWhenProjectIdDoesnotExists()
        {
            project_dto.project_id = 0;
            projectService.Delete(project_dto.project_id);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Delete_WhenCalled_ThrowProjectDoesnotExistsExceptionWhenProjectNameIsNull()
        {
            project_dto.project_name = null;
            projectService.Delete(project_dto.project_id);
        }

    }
}

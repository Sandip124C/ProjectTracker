﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Base.TransactionScopeHelper
{
    public class TransactionScopeHelper : SessionFactory.SessionFactory, Interfaces.TransactionScopeHelper
    {
        public static int count = 0;
        public TransactionScope getTransactionScope() {
            count++;
            return new TransactionScope(TransactionScopeOption.Required);
        }

        public void complete(TransactionScope tx) {
            count--;
            if (count == 0) {
                flushAndDisposeSession();
            }
            tx.Complete();
        }

        public void rollbackTransaction() {
            count = 0;
            disposeSession();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Base.TransactionScopeHelper.Interfaces
{
    public interface TransactionScopeHelper
    {
        TransactionScope getTransactionScope();
        void complete(TransactionScope tx);
        void rollbackTransaction();

    }
}

﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace Base.EntityManager
{
    public class EntityManager<T> :SessionFactory.SessionFactory where T : class
    {
        
        public void persist(T entities) {
            ISession session = getCurrentSession();
            session.SaveOrUpdate(entities);
        }

        public void delete(T entities) {
            ISession session = getCurrentSession();
            session.Delete(entities);
        }

        public void flush() {
            ISession session = getCurrentSession();
            session.Flush();
        }
    }
}

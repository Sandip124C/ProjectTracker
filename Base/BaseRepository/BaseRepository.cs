﻿using System.Collections.Generic;
using NHibernate;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;

namespace Base.BaseRepository
{
    public class BaseRepository<T> : Interfaces.BaseRepository<T> where T : class
    {
        TransactionScopeHelperInterface tansactionScopeHelper;

        public BaseRepository(TransactionScopeHelperInterface _tansactionScopeHelper) {
            tansactionScopeHelper = _tansactionScopeHelper;
        }

        public void delete(T entities) {
            ISession session = SessionFactory.SessionFactory.getCurrentSession();
            session.Delete(entities);


        }
        public void insert(T entities) {

            ISession session = SessionFactory.SessionFactory.getCurrentSession();

            session.Save(entities);

        }
        public void update(T entities) {

            ISession session = SessionFactory.SessionFactory.getCurrentSession();

            session.Update(entities);

        }
        public IList<T> getAll() {
            IList<T> lists;
            ISession session = SessionFactory.SessionFactory.getCurrentSession();
            lists = session.QueryOver<T>().List<T>();
            return lists;
        }
        public IQueryOver<T, T> getQueryable() {
            ISession session = SessionFactory.SessionFactory.getCurrentSession();
            return session.QueryOver<T>();
        }

        public T getById(int id) {
            ISession session = SessionFactory.SessionFactory.getCurrentSession();
            return session.Get<T>(id);
        }
    }
}

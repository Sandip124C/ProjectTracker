﻿using System;
using System.Collections.Generic;
using System.Text;
using NHibernate;

namespace Base.BaseRepository.Interfaces
{
    public interface BaseRepository<T> where T : class
    {
        void delete(T entities);
        void insert(T entities);
        void update(T entities);
        IList<T> getAll();
        IQueryOver<T, T> getQueryable();
        T getById(int id);
    }
}

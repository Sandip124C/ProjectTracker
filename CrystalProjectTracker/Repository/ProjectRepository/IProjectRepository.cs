﻿using CrystalProjectTracker.Models;
using Interfaces = Base.BaseRepository.Interfaces;

namespace CrystalProjectTracker.Repository.ProjectRepository
{
    public interface IProjectRepository: Interfaces.BaseRepository<Project>
    {
        Project GetProjectById(int id);
        Project GetProjectByName(string name);
    }
}

﻿using Base.BaseRepository;
using CrystalProjectTracker.Models;
using NHibernate;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;
using Base.SessionFactory;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Repository.ProjectRepository
{
    public class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {
        public ProjectRepository(TransactionScopeHelperInterface _transactionScopeHelper) : base(_transactionScopeHelper)
        {

        }

        public Project GetProjectById(int id)
        {
            Project project;
            ISession session = SessionFactory.getCurrentSession();
            project = session.QueryOver<Project>().Where(a => a.project_id == id).SingleOrDefault();
            return project;
        }

        public Project GetProjectByName(string name)
        {
            Project project;
            ISession session = SessionFactory.getCurrentSession();
            project = session.QueryOver<Project>().Where(a => a.project_name == name).SingleOrDefault();
            return project;
        }
    }
}


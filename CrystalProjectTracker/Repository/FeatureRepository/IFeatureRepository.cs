﻿using CrystalProjectTracker.Models;
using System.Collections.Generic;
using Interfaces = Base.BaseRepository.Interfaces;

namespace CrystalProjectTracker.Repository.FeatureRepository
{
    public interface IFeatureRepository : Interfaces.BaseRepository<Feature>
    {
        Feature GetFeatureById(int id);
        Feature GetFeatureByName(string name);
        IList<Feature> GetFeatureByModuleId(int id);
    }
}

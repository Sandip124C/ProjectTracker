﻿using Base.BaseRepository;
using Base.SessionFactory;
using CrystalProjectTracker.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;


namespace CrystalProjectTracker.Repository.FeatureRepository
{
    public class FeatureRepository : BaseRepository<Feature>, IFeatureRepository
    {
        public FeatureRepository(TransactionScopeHelperInterface _transactionScopeHelper) : base(_transactionScopeHelper)
        {

        }
        public Feature GetFeatureById(int id)
        {
            Feature feature;
            ISession session = SessionFactory.getCurrentSession();
            feature = session.QueryOver<Feature>().Where(a => a.feature_id == id).SingleOrDefault();
            return feature;
        }

        public IList<Feature> GetFeatureByModuleId(int id)
        {
            IList<Feature> feature;
            ISession session = SessionFactory.getCurrentSession();
            feature = (from p in session.QueryOver<Feature>()
                       where p.module_id == id
                       select p).List();
            return feature;
        }

        public Feature GetFeatureByName(string name)
        {
            Feature feature;
            ISession session = SessionFactory.getCurrentSession();
            feature = session.QueryOver<Feature>().Where(a => a.feature_name == name).SingleOrDefault();
            return feature;
        }
    }
}

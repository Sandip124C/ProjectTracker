﻿using Base.BaseRepository;
using Base.SessionFactory;
using CrystalProjectTracker.Models;
using NHibernate;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;


namespace CrystalProjectTracker.Repository.UserRepository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(TransactionScopeHelperInterface _transactionScopeHelper) : base(_transactionScopeHelper)
        {

        }

        public User GetUserById(int id)
        {
            User user;
            ISession session = SessionFactory.getCurrentSession();
            user = session.QueryOver<User>().Where(a => a.user_id == id).SingleOrDefault();
            return user;
        }

        public User GetUserByEmail(string email)
        {
            User user;
            ISession session = SessionFactory.getCurrentSession();
            user = session.QueryOver<User>().Where(a => a.user_email == email).SingleOrDefault();
            return user;
        }
    }
}

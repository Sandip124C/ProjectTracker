﻿using CrystalProjectTracker.Models;
using Interfaces = Base.BaseRepository.Interfaces;

namespace CrystalProjectTracker.Repository.UserRepository
{
    public interface IUserRepository : Interfaces.BaseRepository<User>
    {
        User GetUserById(int id);
        User GetUserByEmail(string email);
    }
}

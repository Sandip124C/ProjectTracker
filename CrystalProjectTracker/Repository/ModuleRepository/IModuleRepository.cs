﻿using CrystalProjectTracker.Models;
using System.Collections.Generic;
using Interfaces = Base.BaseRepository.Interfaces;

namespace CrystalProjectTracker.Repository.ModuleRepository
{
    public interface IModuleRepository : Interfaces.BaseRepository<Module>
    {
        Module GetModuleById(int id);
        Module GetModuleByName(string name);
        IList<Module> GetModuleByProjectId(int id);
    }
}

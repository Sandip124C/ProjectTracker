﻿using Base.BaseRepository;
using Base.SessionFactory;
using CrystalProjectTracker.Models;
using NHibernate;
using System.Collections.Generic;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;

namespace CrystalProjectTracker.Repository.ModuleRepository
{
    public class ModuleRepository : BaseRepository<Module>, IModuleRepository
    {
        public ModuleRepository(TransactionScopeHelperInterface _transactionScopeHelper) : base(_transactionScopeHelper)
        {

        }
        public Module GetModuleById(int id)
        {
            Module module;
            ISession session = SessionFactory.getCurrentSession();
            module = session.QueryOver<Module>().Where(a => a.module_id == id).SingleOrDefault();
            return module;
        }

        public Module GetModuleByName(string name)
        {
            Module module;
            ISession session = SessionFactory.getCurrentSession();
            module = session.QueryOver<Module>().Where(a => a.module_name == name).SingleOrDefault();
            return module;
        }

        public IList<Module> GetModuleByProjectId(int id)
        {
            IList<Module> module;
            ISession session = SessionFactory.getCurrentSession();
            module = (from p in session.QueryOver<Module>()
                            where p.project_id == id
                            select p).List();
            return module;
        }
    }
}

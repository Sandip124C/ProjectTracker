﻿using ClientNotifications;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Services.DTO;
using CrystalProjectTracker.Services.ProjectService;
using CrystalProjectTracker.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using static ClientNotifications.Helpers.NotificationHelper;


namespace CrystalProjectTracker.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IClientNotification _clientNotification;
        private readonly IProjectService _projectService;
        private readonly IProjectRepository _projectRepository;

        public ProjectController(IProjectRepository projectRepository,IProjectService projectService,IClientNotification clientNotification)
        {
            _clientNotification = clientNotification;
            _projectRepository = projectRepository;
            _projectService = projectService;
        }

        public ActionResult Index()
        {
            var project_list = _projectRepository.getAll();
            var model = new ProjectListViewModel()
            {
                projects = project_list,
                projects_count = project_list.Count
            };
            return View(model);
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Project/Create
        [HttpPost]
        public ActionResult Create(ProjectDTO project_dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _projectService.Create(project_dto);
                    _clientNotification.AddToastNotification("Project Successfully Created.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                }
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                return View();
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Project/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var project = _projectRepository.getById(id);
                if (project == null)
                {
                    throw new Exception("Cannot retrive data of that project.");
                }
                return View(project);
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Project/Edit/5
        [HttpPost]
        public ActionResult Edit(ProjectDTO project_dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _projectService.Update(project_dto);
                    _clientNotification.AddToastNotification("Project Successfully Updated.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                }
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }

        // POST: Project/Delete
        [HttpPost]
        public ActionResult Delete(int project_id)
        {
            try
            {
                _projectService.Delete(project_id);
                _clientNotification.AddToastNotification("Project Successfully Deleted.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
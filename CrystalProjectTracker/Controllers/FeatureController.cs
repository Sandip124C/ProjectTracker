﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.SessionFactory;
using ClientNotifications;
using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.FeatureRepository;
using CrystalProjectTracker.Repository.ModuleRepository;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Services.DTO;
using CrystalProjectTracker.Services.FeatureService;
using CrystalProjectTracker.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using static ClientNotifications.Helpers.NotificationHelper;

namespace CrystalProjectTracker.Controllers
{
    public class FeatureController : Controller
    {
        private IFeatureRepository _featureRepository;
        private IFeatureService _featureService;
        private IModuleRepository _moduleRepository;
        private IProjectRepository _projectRepository;
        private IClientNotification _clientNotification;

        public FeatureController(IFeatureRepository featureRepository,IFeatureService featureService, IModuleRepository moduleRepository,IProjectRepository projectRepository, IClientNotification clientNotification)
        {
            _featureRepository = featureRepository;
            _featureService = featureService;
            _moduleRepository = moduleRepository;
            _projectRepository = projectRepository;
            _clientNotification = clientNotification;
        }
        // GET: Feature
        public ActionResult Index(int project_id = 0,int module_id = 0)
        {
            IList<Feature> feature_list;
            var project = new List<Project>();
            var module = new List<Module>();

            project.AddRange(_projectRepository.getAll());

            if (project_id > 0)
            {
                module.AddRange(_moduleRepository.GetModuleByProjectId(project_id));
                feature_list = _featureRepository.GetFeatureByModuleId(module_id);
            }
            else
            {
                module.AddRange(_moduleRepository.getAll());
                feature_list = _featureRepository.getAll();
            }
                

            ViewBag.projects = new SelectList(project, "project_id", "project_name");

            
            ViewBag.modules = new SelectList(module, "module_id", "module_name");

            var model = new FeatureListViewModel()
            {
                features = feature_list.ToList(),
                features_count = feature_list.Count
            };
            return View(model);
        }


        // GET: Feature/Create
        public ActionResult Create(int project_id = 0)
        {
            var project = new List<Project>();
            var module = new List<Module>();

            project.AddRange(_projectRepository.getAll());

            ViewBag.projects = new SelectList(project, "project_id", "project_name");

            return View();
        }

        // POST: Feature/Create
        [HttpPost]
        public ActionResult Create(FeatureDTO feature_dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _featureService.Create(feature_dto);
                    _clientNotification.AddToastNotification("Feature Successfully Created.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                }
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Feature/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var project = new List<Project>();
                var module = new List<Module>();

                project.AddRange(_projectRepository.getAll());
                ViewBag.projects = new SelectList(project, "project_id", "project_name");

                var feature = _featureRepository.getById(id);
                if (feature == null)
                {
                    throw new Exception("Cannot retrive data of that feature.");
                }
                return View(feature);
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        public ActionResult Edit(FeatureDTO feature_dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _featureService.Update(feature_dto);
                    _clientNotification.AddToastNotification("Feature Successfully Updated.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                }
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public ActionResult Delete(int feature_id)
        {
            try
            {
                _featureService.Delete(feature_id);
                _clientNotification.AddToastNotification("Feature Successfully Deleted.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult getModulesFromProject([FromBody]ModuleDTO module_dto)
        {
            try
            {
                List<ModuleDTO> modulelist = new List<ModuleDTO>();
                List<Module> modules = _moduleRepository.GetModuleByProjectId(module_dto.project_id).ToList();
                foreach (var module in modules)
                {
                    modulelist.Add(new ModuleDTO() { module_id = module.module_id, module_name = module.module_name });
                }
                return Json(modulelist);
            }
            catch (Exception)
            {
                string responseMessage = "There was Problem while performing the operation";
                return NotFound(Json(new { success = false, responseText = responseMessage }));
            }
        }
    }
}
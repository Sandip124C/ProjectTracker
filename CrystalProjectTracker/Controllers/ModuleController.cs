﻿using ClientNotifications;
using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.ModuleRepository;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Services.DTO;
using CrystalProjectTracker.Services.ModuleService;
using CrystalProjectTracker.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using static ClientNotifications.Helpers.NotificationHelper;

namespace CrystalProjectTracker.Controllers
{
    public class ModuleController : Controller
    {
        private IClientNotification _clientNotification;
        private IModuleRepository _moduleRepository;
        private IProjectRepository _projectRepository;
        private IModuleService _moduleService;

        public ModuleController(IModuleRepository moduleRepository, IProjectRepository projectRepository,IModuleService moduleService,IClientNotification clientNotification)
        {
            _clientNotification = clientNotification;
            _moduleRepository = moduleRepository;
            _projectRepository = projectRepository;
            _moduleService = moduleService;
        }
        // GET: Module
        public ActionResult Index(int project_id = 0)
        {
            IList<Module> module_list;

            if (_moduleRepository.GetModuleByProjectId(project_id) != null && project_id > 0)
            {
                module_list = _moduleRepository.GetModuleByProjectId(project_id);
            }
            else
            {
                module_list = _moduleRepository.getAll();
            }
            var project = new List<Project>();
            project.AddRange(_projectRepository.getAll());
            ViewBag.projects = new SelectList(project, "project_id", "project_name");

            var model = new ModuleListViewModel()
            {
                modules = module_list.ToList(),
                modules_count = module_list.Count
            };
            return View(model);
        }

        // GET: Module/Create
        public ActionResult Create()
        {
            var project = new List<Project>();
            project.AddRange(_projectRepository.getAll());
            ViewBag.projects = new SelectList(project, "project_id", "project_name");

            return View();
        }

        // POST: Module/Create
        [HttpPost]
        public ActionResult Create(ModuleDTO module_dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _moduleService.Create(module_dto);
                    _clientNotification.AddToastNotification("Module Successfully Created.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                }
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Module/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var project = new List<Project>();
                project.AddRange(_projectRepository.getAll());
                ViewBag.projects = new SelectList(project, "project_id", "project_name");

                var module = _moduleRepository.getById(id);
                if(module == null)
                {
                    throw new Exception("Cannot retrive data of that module.");
                }
                return View(module);
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Module/Edit/5
        [HttpPost]
        public ActionResult Edit(ModuleDTO module_dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _moduleService.Update(module_dto);
                    _clientNotification.AddToastNotification("Module Successfully Updated.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                }
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }

        // POST: Module/Delete/5
        [HttpPost]
        public ActionResult Delete(int module_id)
        {
            try
            {
                _moduleService.Delete(module_id);
                _clientNotification.AddToastNotification("Module Successfully Deleted.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
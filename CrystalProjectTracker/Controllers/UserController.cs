﻿using System;
using ClientNotifications;
using CrystalProjectTracker.Repository.ModuleRepository;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Repository.UserRepository;
using CrystalProjectTracker.Services.DTO;
using CrystalProjectTracker.Services.UserService;
using CrystalProjectTracker.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static ClientNotifications.Helpers.NotificationHelper;


namespace CrystalProjectTracker.Controllers
{
    public class UserController : Controller
    {
        private readonly IClientNotification _clientNotification;
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;

        public UserController(IUserRepository userRepository, IClientNotification clientNotification,IUserService userService)
        {
            _clientNotification = clientNotification;
            _userRepository = userRepository;
            _userService = userService;
        }

        // GET: User
        public ActionResult Index()
        {
            var user_list = _userRepository.getAll();
            var model = new UserListViewModel()
            {
                users = user_list,
                users_count = user_list.Count
            };
            return View(model);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(UserDTO user_dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userService.Create(user_dto);
                    _clientNotification.AddToastNotification("User Successfully Created.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                }
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                return View();
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var user = _userRepository.getById(id);
                return View(user);
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
                return RedirectToAction(nameof(Index));
            }
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // POST: User/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
                _clientNotification.AddToastNotification("User Successfully Deleted.", NotificationType.success, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            catch (Exception e)
            {
                _clientNotification.AddToastNotification(e.Message.ToString(), NotificationType.error, new ToastNotificationOption { PositionClass = "toast-top-right", CloseButton = true });
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
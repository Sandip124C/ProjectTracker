﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrystalProjectTracker.Models
{
    public class Module
    {
        [Display(Name = "Module Id")]
        public virtual int module_id { get; set; }

        [Display(Name = "Project Id")]
        public virtual int project_id { get; set; }

        private string ModuleName { get; set; }

        private int ModuleStatus { get; set; }

        private string ModuleDescription { get; set; }

        [Display(Name = "Created")]
        public virtual DateTime created_at { get; set; }

        public virtual Project project { get; set; }

        public virtual ICollection<Feature> features { get; set; }

        [Display(Name = "Module Name")]
        public virtual string module_name
        {
            get => ModuleName;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("Module Name Should not be empty");
                }
                ModuleName = value;
            }
        }

        [Display(Name = "Module Status")]
        public virtual int module_status
        {
            get => ModuleStatus;
            set
            {
                if (value < 0 || value > 1)
                {
                    throw new Exception("Module Status cannot be except 0 and 1");
                }
                ModuleStatus = value;
            }
        }

        [Display(Name = "Module Description")]
        public virtual string module_description
        {
            get => ModuleDescription;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("Module description cannot be empty");
                }
                ModuleDescription = value;
            }
        }

    }
}

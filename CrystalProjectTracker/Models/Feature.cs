﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrystalProjectTracker.Models
{
    public class Feature
    {
        [Display(Name = "Feature Id")]
        public virtual int feature_id { get; set; }

        [Display(Name = "Module Id")]
        public virtual int module_id { get; set; }

        [Display(Name = "Feature Name")]
        public virtual string feature_name { get; set; }

        [Display(Name = "Feature Status")]
        public virtual int feature_status { get; set; }

        [Display(Name = "Feature Description")]
        public virtual string feature_description { get; set; }

        [Display(Name = "Created")]
        public virtual DateTime created_at { get; set; }

        public virtual Module module { get; set; }
    }
}

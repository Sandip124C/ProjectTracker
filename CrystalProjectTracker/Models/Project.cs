﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CrystalProjectTracker.Models
{
    public class Project
    {
        [Display(Name = "Project Id")]
        public virtual int project_id { get; set; }

        private string ProjectName { get; set; }

        private int ProjectStatus { get; set; }

        private string ProjectDescription { get; set; }

        [Display(Name = "Created")]
        public virtual DateTime created_at { get; set; }

        public virtual ICollection<Module> modules { get; set; }

        [Display(Name = "Project Name")]

        public virtual string project_name
        {
            get => ProjectName;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("Project Name Should not be empty");
                }
                ProjectName = value;
            }
        }

        [Display(Name = "Project Status")]
        public virtual int project_status
        {
            get => ProjectStatus;
            set
            {
                if(value < 0 || value > 1)
                {
                    throw new Exception("Project Status cannot be except 0 and 1");
                }
                ProjectStatus = value;
            }
        }

        [Display(Name = "Project Description")]
        public virtual string project_description
    {
            get => ProjectDescription;
            set
            {
                if(string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("Project description cannot be empty");
                }
                ProjectDescription = value;
            }
        }
    }
}

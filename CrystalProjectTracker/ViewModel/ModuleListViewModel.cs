﻿using CrystalProjectTracker.Models;
using System.Collections.Generic;

namespace CrystalProjectTracker.ViewModel
{
    public class ModuleListViewModel
    {
        public IList<Module> modules { get; set; }
        public int modules_count { get; set; }
    }
}

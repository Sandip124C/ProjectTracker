﻿using CrystalProjectTracker.Models;
using System.Collections.Generic;

namespace CrystalProjectTracker.ViewModel
{
    public class FeatureListViewModel
    {
        public IList<Feature> features { get; set; }
        public int features_count { get; set; }
    }
}

﻿using CrystalProjectTracker.Models;
using System.Collections.Generic;

namespace CrystalProjectTracker.ViewModel
{
    public class ProjectListViewModel
    {
        public IList<Project> projects { get; set; }
        public int projects_count { get; set; }
    }
}

﻿using CrystalProjectTracker.Models;
using System.Collections.Generic;

namespace CrystalProjectTracker.ViewModel
{
    public class UserListViewModel
    {
        public IList<User> users { get; set; }
        public int users_count { get; set; }
    }
}

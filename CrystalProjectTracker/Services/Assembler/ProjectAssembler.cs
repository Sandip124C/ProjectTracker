﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.Assembler
{
    public class ProjectAssembler : IProjectAssembler
    {
        public void copyProjectFromProjectDTO(Project project, ProjectDTO project_dto)
        {
            project.project_id = project_dto.project_id;
            project.project_name = project_dto.project_name;
            project.project_status = project_dto.project_status;
            project.project_description = project_dto.project_description;
        }
    }
}

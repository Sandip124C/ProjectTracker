﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.Assembler.Interface
{
    public interface IModuleAssembler
    {
        void copyModuleFromModuleDTO(Module module, ModuleDTO module_dto);

    }
}

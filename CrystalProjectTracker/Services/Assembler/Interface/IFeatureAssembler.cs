﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.Assembler.Interface
{
    public interface IFeatureAssembler
    {
        void copyFeatureFromFeatureDTO(Feature feature, FeatureDTO feature_dto);
    }
}

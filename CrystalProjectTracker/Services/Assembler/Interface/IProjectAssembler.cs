﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.Assembler.Interface
{
    public interface IProjectAssembler
    {
        void copyProjectFromProjectDTO(Project project, ProjectDTO project_dto);
    }
}

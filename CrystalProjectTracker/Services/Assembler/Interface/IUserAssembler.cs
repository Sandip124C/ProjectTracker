﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.Assembler.Interface
{
    public interface IUserAssembler
    {
        void CopyUserFromUserDTO(User project, UserDTO project_dto);
    }
}

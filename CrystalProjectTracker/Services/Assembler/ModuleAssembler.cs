﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;
using System;

namespace CrystalProjectTracker.Services.Assembler
{
    public class ModuleAssembler : IModuleAssembler
    {
        private IProjectRepository _projectRepository;
        public ModuleAssembler(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }
        public void copyModuleFromModuleDTO(Module module, ModuleDTO module_dto)
        {
            module.module_id = module_dto.module_id;
            module.module_name = module_dto.module_name;
            module.project_id = module_dto.project_id;
            module.project = _projectRepository.getById(module_dto.project_id);
            module.module_status = module_dto.module_status;
            module.module_description = module_dto.module_description;
        }
    }
}

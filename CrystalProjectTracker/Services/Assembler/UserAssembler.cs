﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.Assembler
{
    public class UserAssembler : IUserAssembler
    {
        public void CopyUserFromUserDTO(User user, UserDTO user_dto)
        {
            user.user_id = user_dto.user_id;
            user.user_name = user_dto.user_name;
            user.user_email = user_dto.user_email;
            user.user_password = user_dto.user_password;
            user.user_status = user_dto.user_status;
        }
    }
}

﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.ModuleRepository;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;
using System;

namespace CrystalProjectTracker.Services.Assembler
{
    public class FeatureAssembler : IFeatureAssembler
    {

        private IModuleRepository _moduleRepository;
        public FeatureAssembler(IModuleRepository moduleRepository)
        {
            _moduleRepository = moduleRepository;
        }

        public void copyFeatureFromFeatureDTO(Feature feature, FeatureDTO feature_dto)
        {
            feature.feature_id = feature_dto.feature_id;
            feature.feature_name = feature_dto.feature_name;
            feature.feature_description = feature_dto.feature_description;
            feature.feature_status = feature_dto.feature_status;
            feature.module_id = feature_dto.module_id;
            feature.module = _moduleRepository.getById(feature_dto.module_id);
        }
    }
}

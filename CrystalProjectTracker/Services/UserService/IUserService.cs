﻿using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.UserService
{
    public interface IUserService
    {
        void Create(UserDTO user);
        void Update(UserDTO user);
        void Delete(int id);
    }
}

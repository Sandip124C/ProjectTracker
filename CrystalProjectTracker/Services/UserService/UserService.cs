﻿using System;
using System.Transactions;
using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.UserRepository;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;


namespace CrystalProjectTracker.Services.UserService
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        private TransactionScopeHelperInterface _transactionScopeHelper;
        private IUserAssembler _userAssembler;

        public UserService(IUserRepository userRepository, TransactionScopeHelperInterface transactionScopeHelper, IUserAssembler userAssembler)
        {
            _userRepository = userRepository;
            _transactionScopeHelper = transactionScopeHelper;
            _userAssembler = userAssembler;
        }

        public void Create(UserDTO user_dto)
        {
            try
            {
                using (System.Transactions.TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if (_userRepository.GetUserByEmail(user_dto.user_email) != null)
                    {
                        throw new Exception("User with that email already exists.");
                    }
                    User user = new User();
                    copy_user_from_dto(user,user_dto);
                    _userRepository.insert(user);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Delete(int user_id)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if (user_id == 0)
                    {
                        throw new Exception("Invalid User Id");
                    }
                    User anotherUserInstance = _userRepository.GetUserById(user_id);
                    if (anotherUserInstance == null)
                    {
                        throw new Exception("User with that id doesnot exists.");
                    }
                    _userRepository.delete(anotherUserInstance);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Update(UserDTO user_dto)
        {
            try
            {
                using (System.Transactions.TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    User anotherUserInstance = _userRepository.GetUserByEmail(user_dto.user_email);
                    if (anotherUserInstance != null && anotherUserInstance.user_id != user_dto.user_id)
                    {
                        throw new Exception("User with that email already exists.");
                    }
                    User user = new User();
                    copy_user_from_dto(user, user_dto);
                    _userRepository.update(user);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void copy_user_from_dto(User user, UserDTO user_dto)
        {
            _userAssembler.CopyUserFromUserDTO(user, user_dto);
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CrystalProjectTracker.Services.DTO
{
    public class UserDTO
    {
        [Display(Name = "User Id")]
        public virtual int user_id { get; set; }

        [Display(Name = "User Name")]
        public virtual string user_name { get; set; }

        [Display(Name = "User Email")]
        public virtual string user_email { get; set; }

        [Display(Name = "User Password")]
        public virtual string user_password { get; set; }

        [Display(Name = "User Status")]
        public virtual int user_status { get; set; }

        [Display(Name = "Created")]
        public virtual DateTime created_at { get; set; }
    }
}

﻿using CrystalProjectTracker.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace CrystalProjectTracker.Services.DTO
{
    public class ModuleDTO
    {
        [Display(Name = "Module Id")]
        public virtual int module_id { get; set; }

        [Display(Name = "Project Id")]
        public virtual int project_id { get; set; }

        [Display(Name = "Module Name")]
        public virtual string module_name { get; set; }

        [Display(Name = "Module Status")]
        public virtual int module_status { get; set; }

        [Display(Name = "Module Description")]
        public virtual string module_description { get; set; }

        [Display(Name = "Created")]
        public virtual DateTime created_at { get; set; }
    }
}

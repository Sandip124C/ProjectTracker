﻿using CrystalProjectTracker.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CrystalProjectTracker.Services.DTO
{
    public class ProjectDTO
    {
        [Display(Name = "Project Id")]
        public virtual int project_id { get; set; }

        [Display(Name = "Project Name")]
        public virtual string project_name { get; set; }

        [Display(Name = "Project Status")]
        public virtual int project_status { get; set; }

        [Display(Name = "Project Description")]
        public virtual string project_description { get; set; }

        [Display(Name = "Created")]
        public virtual DateTime created_at { get; set; }

    }
}

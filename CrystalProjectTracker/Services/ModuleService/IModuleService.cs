﻿using CrystalProjectTracker.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrystalProjectTracker.Services.ModuleService
{
    public interface IModuleService
    {
        void Create(ModuleDTO module_dto);
        void Update(ModuleDTO module_dto);
        void Delete(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Transactions;
using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.ModuleRepository;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;
using Microsoft.AspNetCore.Mvc;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;


namespace CrystalProjectTracker.Services.ModuleService
{
    public class ModuleService : IModuleService
    {
        private IModuleRepository _moduleRepository;
        private TransactionScopeHelperInterface _transactionScopeHelper;
        private IModuleAssembler _moduleAssembler;

        public ModuleService(IModuleRepository moduleRepository,TransactionScopeHelperInterface transactionScopeHelper, IModuleAssembler moduleAssembler)
        {
            _moduleRepository = moduleRepository;
            _transactionScopeHelper = transactionScopeHelper;
            _moduleAssembler = moduleAssembler;
        }
        public void Create(ModuleDTO module_dto)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if(_moduleRepository.GetModuleByName(module_dto.module_name) != null && _moduleRepository.GetModuleByProjectId(module_dto.project_id) != null)
                    {
                        throw new Exception("Module with that name already exists.");
                    }
                    Module module = new Module();
                    copy_module_from_module_dto(module, module_dto);
                    _moduleRepository.insert(module);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if(id == 0)
                    {
                        throw new Exception("Invalid module Id.");
                    }
                    Module anotherModuleInstance = _moduleRepository.GetModuleById(id);
                    if(anotherModuleInstance == null)
                    {
                        throw new Exception("Module with that id doesnot Exists.");
                    }
                    _moduleRepository.delete(anotherModuleInstance);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Update(ModuleDTO module_dto)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    Module anotherModuleInstance = _moduleRepository.GetModuleByName(module_dto.module_name);
                    if(anotherModuleInstance != null && anotherModuleInstance.module_id != module_dto.module_id)
                    {
                        throw new Exception("Module with that name already exists");
                    }
                    Module module = _moduleRepository.getById(module_dto.module_id);
                    copy_module_from_module_dto(module, module_dto);
                    _moduleRepository.update(module);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void copy_module_from_module_dto(Module module, ModuleDTO module_dto)
        {
            if(module_dto.project_id > 0)
            {
                _moduleAssembler.copyModuleFromModuleDTO(module, module_dto);
            }
        }
    }
}

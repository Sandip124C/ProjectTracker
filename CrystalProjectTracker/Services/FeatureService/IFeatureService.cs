﻿using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.FeatureService
{
    public interface IFeatureService
    {
        void Create(FeatureDTO feature_dto);
        void Update(FeatureDTO feature_dto);
        void Delete(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.FeatureRepository;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;


namespace CrystalProjectTracker.Services.FeatureService
{
    public class FeatureService : IFeatureService
    {
        private IFeatureRepository _featureRepository;
        private TransactionScopeHelperInterface _transactionScopeHelper;
        private IFeatureAssembler _featureAssembler;

        public FeatureService(IFeatureRepository featureRepository, TransactionScopeHelperInterface transactionScopeHelper, IFeatureAssembler featureAssembler)
        {
            _featureRepository = featureRepository;
            _transactionScopeHelper = transactionScopeHelper;
            _featureAssembler = featureAssembler;
        }
        public void Create(FeatureDTO feature_dto)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if(_featureRepository.GetFeatureByName(feature_dto.feature_name) != null && _featureRepository.GetFeatureByModuleId(feature_dto.module_id) != null)
                    {
                        throw new Exception("Feature with that name already exists.");
                    }
                    Feature feature = new Feature();
                    copy_feature_from_feature_dto(feature, feature_dto);
                    _featureRepository.insert(feature);
                    _transactionScopeHelper.complete(tx);

                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if(id == 0)
                    {
                        throw new Exception("Invalid feature id.");
                    }
                    Feature anotherFeatureInstance = _featureRepository.GetFeatureById(id);
                    if(anotherFeatureInstance == null)
                    {
                        throw new Exception("Feature with that id doesnot exists.");
                    }
                    _featureRepository.delete(anotherFeatureInstance);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Update(FeatureDTO feature_dto)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    Feature anotherFeatureInstance = _featureRepository.GetFeatureByName(feature_dto.feature_name);
                    if(anotherFeatureInstance != null && anotherFeatureInstance.feature_id != feature_dto.feature_id)
                    {
                        throw new Exception("Feature with that name already exists.");
                    }
                    Feature feature = _featureRepository.getById(feature_dto.feature_id);
                    copy_feature_from_feature_dto(feature ,feature_dto);
                    _featureRepository.update(feature);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void copy_feature_from_feature_dto(Feature feature, FeatureDTO feature_dto)
        {
            if(feature_dto.module_id > 0)
            {
                _featureAssembler.copyFeatureFromFeatureDTO(feature, feature_dto);
            }
        }
    }
}

﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.DTO;
using System;
using System.Transactions;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;


namespace CrystalProjectTracker.Services.ProjectService
{
    public class ProjectService : IProjectService
    {
        private IProjectRepository _projectRepository;
        private TransactionScopeHelperInterface _transactionScopeHelper;
        private IProjectAssembler _projectAssembler;

        public ProjectService(IProjectRepository projectRepository, TransactionScopeHelperInterface transactionScopeHelper, IProjectAssembler projectAssembler)
        {
            _projectRepository = projectRepository;
            _transactionScopeHelper = transactionScopeHelper;
            _projectAssembler = projectAssembler;
        }

        public void Create(ProjectDTO project_dto)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if (_projectRepository.GetProjectByName(project_dto.project_name) != null)
                    {
                        throw new Exception("Project with that name already exists.");
                    }
                    Project project = new Project();
                    copy_project_from_dto(project, project_dto);
                    _projectRepository.insert(project);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Delete(int project_id)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    if (project_id == 0)
                    {
                        throw new Exception("Invalid Project Id");
                    }
                    Project anotherProjectInstance = _projectRepository.GetProjectById(project_id);
                    if (anotherProjectInstance == null)
                    {
                        throw new Exception("Project with that id doesnot exists.");
                    }
                    _projectRepository.delete(anotherProjectInstance);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void Update(ProjectDTO project_dto)
        {
            try
            {
                using (TransactionScope tx = _transactionScopeHelper.getTransactionScope())
                {
                    Project anotherProjectInstance = _projectRepository.GetProjectByName(project_dto.project_name);
                    if (anotherProjectInstance != null && anotherProjectInstance.project_id != project_dto.project_id)
                    {
                        throw new Exception("Project with that name already exists.");
                    }
                    Project project = _projectRepository.getById(project_dto.project_id);
                    copy_project_from_dto(project, project_dto);
                    _projectRepository.update(project);
                    _transactionScopeHelper.complete(tx);
                }
            }
            catch (Exception)
            {
                _transactionScopeHelper.rollbackTransaction();
                throw;
            }
        }

        public void copy_project_from_dto(Project project,ProjectDTO project_dto)
        {
            _projectAssembler.copyProjectFromProjectDTO(project, project_dto);
        }

    }
}

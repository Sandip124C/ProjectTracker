﻿using CrystalProjectTracker.Models;
using CrystalProjectTracker.Services.DTO;

namespace CrystalProjectTracker.Services.ProjectService
{
    public interface IProjectService
    {
        void Create(ProjectDTO project);
        void Update(ProjectDTO project);
        void Delete(int id);
    }
}

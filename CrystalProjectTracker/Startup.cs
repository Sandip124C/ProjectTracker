﻿using Base.SessionFactory;
using Base.TransactionScopeHelper;
using ClientNotifications.ServiceExtensions;
using CrystalProjectTracker.Repository.FeatureRepository;
using CrystalProjectTracker.Repository.ModuleRepository;
using CrystalProjectTracker.Repository.ProjectRepository;
using CrystalProjectTracker.Repository.UserRepository;
using CrystalProjectTracker.Services.Assembler;
using CrystalProjectTracker.Services.Assembler.Interface;
using CrystalProjectTracker.Services.FeatureService;
using CrystalProjectTracker.Services.ModuleService;
using CrystalProjectTracker.Services.ProjectService;
using CrystalProjectTracker.Services.UserService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Linq;
using TransactionScopeHelperInterface = Base.TransactionScopeHelper.Interfaces.TransactionScopeHelper;


namespace CrystalProjectTracker
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            configureProjectTrackerConfigs(services);

            services.AddToastNotification();
            services.AddScoped(typeof(Base.BaseRepository.Interfaces.BaseRepository<>), typeof(Base.BaseRepository.BaseRepository<>));

            services.AddMvc();

            //for compressing data
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] { "imagejpeg", "png", "jpeg", "jpg" });
            });
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            var httpAccessor = serviceProvider.GetService<IHttpContextAccessor>();
            SessionFactory.httpContextAccessor = httpAccessor;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        public void configureProjectTrackerConfigs(IServiceCollection services)
        {
            configureProjectServices(services);
            configureProjectRepositories(services);
            configureProjectAssembler(services);
            configureProjectTrackerLibrary(services);
        }

        public void configureProjectServices(IServiceCollection services)
        {
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IModuleService, ModuleService>();
            services.AddScoped<IFeatureService, FeatureService>();
        }
        public void configureProjectRepositories(IServiceCollection services)
        {
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IModuleRepository, ModuleRepository>();
            services.AddScoped<IFeatureRepository, FeatureRepository>();
        }
        public void configureProjectTrackerLibrary(IServiceCollection services)
        {
            services.AddScoped<TransactionScopeHelperInterface, TransactionScopeHelper>();
        }
        public void configureProjectAssembler(IServiceCollection services)
        {
            services.AddScoped<IProjectAssembler, ProjectAssembler>();
            services.AddScoped<IUserAssembler, UserAssembler>();
            services.AddScoped<IModuleAssembler, ModuleAssembler>();
            services.AddScoped<IFeatureAssembler, FeatureAssembler>();
        }

    }
}
